ics-ans-role-desktop-base
=========================

Ansible role to install basic files required by desktop applications,
like ESS desktop menu and ESS links.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
desktop_base_ess_links:
  - name: "JupyterHub"
    description: "ESS JupyterHub"
    url: https://jupyterhub.esss.lu.se
  - name: "Logbook"
    description: "ESS Logbook"
    url: https://logbook.esss.lu.se
  - name: "ESS Confluence"
    description: "ESS Confluence website"
    url: https://confluence.esss.lu.se
  - name: "JIRA CS-Studio"
    description: "CS-Studio JIRA project"
    url: https://jira.esss.lu.se/projects/CSSTUDIO
  - name: "JIRA OpenXAL"
    description: "OpenXAL JIRA project"
    url: https://jira.esss.lu.se/projects/OXAL
  - name: "JIRA ICS Infrastructure"
    description: "ICS Infrastructure JIRA project"
    url: https://jira.esss.lu.se/projects/INFRA
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-desktop-base
```

License
-------

BSD 2-clause
